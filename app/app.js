var app = angular.module('7movies', []);

app.controller('mainController',['$http', function($http) {
    this.title = '7movies';
    this.init = function() {
        var promise = $http.get('http://www.omdbapi.com/?t=Game%20of%20Thrones&Season=1');
        promise.then(function(movie) {
            console.log(movie.data);
            this.movie = movie.data;
        }.bind(this));
    };
}]);
